package com.example.spa_chat.base_classes;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

//implementing base service, getting T as repository, V as class
public abstract class BaseService<V,T extends BaseRepository<V>> {

    @Autowired
    protected T repository;

    public V update(V item){ return repository.save(item); }

    public void delete(V item){
        repository.delete(item);
    }

    public List<V> getAll(){
        return repository.findAll();
    }

    public V add(V item){
        return repository.save(item);
    }
}
