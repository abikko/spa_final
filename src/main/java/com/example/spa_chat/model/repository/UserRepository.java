package com.example.spa_chat.model.repository;

import com.example.spa_chat.base_classes.BaseRepository;
import com.example.spa_chat.model.pojo.User;

public interface UserRepository extends BaseRepository<User> {
    User findUserById(Long id);
}
