package com.example.spa_chat.model.repository;

import com.example.spa_chat.base_classes.BaseRepository;
import com.example.spa_chat.model.pojo.Auth;

public interface AuthRepository extends BaseRepository<Auth> {
    Auth findAuthByLoginAndPassword(String login, String password);

    Auth findAuthByToken(String token);
}
