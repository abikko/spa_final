package com.example.spa_chat.model.repository;

import com.example.spa_chat.base_classes.BaseRepository;
import com.example.spa_chat.model.pojo.Messages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessagesRepository extends BaseRepository<Messages> {
}
