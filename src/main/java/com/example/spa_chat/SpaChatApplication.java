package com.example.spa_chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpaChatApplication.class, args);
    }

}
