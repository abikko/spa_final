package com.example.spa_chat.service;

import com.example.spa_chat.base_classes.BaseService;
import com.example.spa_chat.model.pojo.Chat;
import com.example.spa_chat.model.repository.ChatRepository;

public class ChatService extends BaseService<Chat, ChatRepository> {

}
