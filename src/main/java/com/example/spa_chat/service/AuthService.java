package com.example.spa_chat.service;

import com.example.spa_chat.base_classes.BaseService;
import com.example.spa_chat.model.pojo.Auth;
import com.example.spa_chat.model.pojo.User;
import com.example.spa_chat.model.repository.AuthRepository;
import com.example.spa_chat.model.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
@AllArgsConstructor
public class AuthService extends BaseService<Auth, AuthRepository> {

    @Autowired
    private UserRepository userRepository;

    public User signIn(String login,String password) {
        return userRepository.getById(repository.findAuthByToken(findToken(login, password)).getUserId());
    }

    public User signUp(String login, String password, String name){
        User newUser = new User(name);
        Auth auth = new Auth(newUser.getId(),login,password,UUID.randomUUID().toString());
        userRepository.save(newUser);
        add(auth);
        return newUser;
    }


    private String findToken(String login, String password){
        return repository.findAuthByLoginAndPassword(login, password).getToken();
    }
}
