package com.example.spa_chat.service;

import com.example.spa_chat.base_classes.BaseService;
import com.example.spa_chat.model.pojo.User;
import com.example.spa_chat.model.repository.UserRepository;

public class UserService extends BaseService<User, UserRepository> {

}
