package com.example.spa_chat.controller;

import com.example.spa_chat.base_classes.BaseController;
import com.example.spa_chat.model.pojo.Auth;
import com.example.spa_chat.model.repository.AuthRepository;
import com.example.spa_chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController extends BaseController<Auth, AuthRepository, AuthService> {

    @RequestMapping("/signin/{login}/{password}")
    public ResponseEntity<?> signIn(@PathVariable(value = "login") String login, @PathVariable(value = "password") String password){
        return ResponseEntity.ok(service.signIn(login,password));
    }

    @RequestMapping("/signup/{login}/{password}/{name}")
    public ResponseEntity<?> signUp(
            @PathVariable(value = "login") String login,
            @PathVariable(value = "password") String password,
            @PathVariable(value = "name") String name
    ){
        return ResponseEntity.ok(service.signUp(login,password,name));
    }
}
